﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage2 : ContentPage
    {
        public MainPage2()
        {
            InitializeComponent();
        }
        private async void OnButtonClickedNo2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new No());
        }
        private async void OnButtonClickedThink2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Think());
        }
        private async void OnButtonClickedYes2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Yes());
        }
        private async void OnButtonClicked2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage3());
        }
    }
}