﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Nope : ContentPage
    {
        public Nope()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
        protected override void OnDisappearing()
        {
            DisplayAlert("I SEE YOU", "I know you were wrong ... be careful ..", "OK");
        }
    }
}