﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage3 : ContentPage
    {
        public MainPage3()
        {
            InitializeComponent();
        }
        private async void OnButtonClickedImage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage4());
        }
        private async void Nope(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Nope());
        }
    }
}