﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage5 : ContentPage
    {
        public MainPage5()
        {
            InitializeComponent();
        }
        private void submit_Clicked(object sender, EventArgs e) // get input of user and chck if the answer is right
        {
            var nameValue = name.Text;
            if (name.Text == "Barack Obama" || name.Text == "Obama" || name.Text == "Barack" || name.Text == "barack obama" || name.Text == "obama" || name.Text == "barack") 
            {
                Navigation.PushAsync(new MainPage6()); //win
            }
            else
            {
                Navigation.PushAsync(new No()); //lose
            }
        }
        protected override void OnAppearing()
        {
            DisplayAlert("STOP", "Are you sure you are ready for the last question ?????", "YES, I'm sure");
        }
    }
}